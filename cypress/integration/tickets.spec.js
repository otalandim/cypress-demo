describe("Tickets", () => {
  beforeEach(() =>
    cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html")
  );

  it("should fill all the next input fields", () => {
    const infosUser = {
      firstName: "Otávio",
      lastName: "Landim",
      email: "teste@email.com",
      requests: "Testando agora",
      signature: "Otávio Landim"
    };

    cy.infoUser(infosUser);
  });

  it("should verify if was selected option two to ticket groups", () => {
    cy.get("#ticket-quantity").select("2");
  });

  it("should verify if radio button was checked", () => {
    cy.get("#vip").check();
  });

  it("should verify if checkbox was checked", () => {
    cy.get("#social-media").check();
  });

  it("should verify all checkboxs was checked and after uncheecked to last element", () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#social-media").check();

    cy.get("#social-media").uncheck();
  });

  it("should verify if the title of page is TICKETBOX", () => {
    cy.get("header h1").should("contain", "TICKETBOX");
  });

  it("should verify if email is invalid", () => {
    cy.get("#email")
      .as("email")
      .type("emailinvalid-email.com");

    cy.get("#email.invalid").should("exist");

    cy.get("@email")
      .clear()
      .type("email@email.com");

    cy.get("#email.invalid").should("not.exist");
  });
});
